# Advent Of Code 2023

[![Advent Of Code](https://img.shields.io/badge/Advent%20Of%20Code-2023-8803ec.svg?style=flat-square)](https://adventofcode.com/2023)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

![AOC 2023](./AOC-2023.png "AOC 2023")

In this repo I will post every solution of the Advent Of Code I will do.

Most of the solutions will be written in JS and executed with Bun.

You can join the challenge too at [adventofcode.com](https://adventofcode.com/2023)


