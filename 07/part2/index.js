const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();
/*
content = `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483`
*/
content = content.trim();

let cardLabels = [
  "A",
  "K",
  "Q",
  "T",
  "9",
  "8",
  "7",
  "6",
  "5",
  "4",
  "3",
  "2",
  "J",
];

let lines = content.split("\n");

let hands = [];

for (let i = 0; i < lines.length; i++) {
  let row = lines[i].split(" ");
  let cards = row[0].split("");
  let bidding = row[1];

  let hand = cards.reduce((prev, curr) => {
    if (!prev[curr]) {
      prev[curr] = 1;
    } else {
      prev[curr] += 1;
    }
    return prev;
  }, {});

  let jokers = 0;

  if (hand.J) {
    jokers = hand.J;
    hand.J = 0;
  }

  // sort hand by count
  hand = Object.entries(hand).sort((a, b) => {
    return b[1] - a[1];
  });

  hand[0][1] += jokers;

  if (hand[0][1] === 5) {
    hand = 6;
  } else if (hand[0][1] === 4) {
    hand = 5;
  } else if (hand[0][1] === 3 && hand[1][1] === 2) {
    hand = 4;
  } else if (hand[0][1] === 3) {
    hand = 3;
  } else if (hand[0][1] === 2 && hand[1][1] === 2) {
    hand = 2;
  } else if (hand[0][1] === 2) {
    hand = 1;
  } else {
    hand = 0;
  }

  hands.push({ cards, bidding, hand });
}

const checkCards = (card1, card2, index) => {
  if (card1[index] === card2[index]) {
    return checkCards(card1, card2, index + 1);
  } else {
    return cardLabels.indexOf(card2[index]) - cardLabels.indexOf(card1[index]);
  }
};

hands = hands.sort((hand1, hand2) => {
  if (hand1.hand === hand2.hand) {
    return checkCards(hand1.cards, hand2.cards, 0);
  } else {
    return hand1.hand - hand2.hand;
  }
});

let totalWinnings = hands.reduce((prev, curr, index) => {
  return prev + curr.bidding * (index + 1);
}, 0);

console.log(totalWinnings);
