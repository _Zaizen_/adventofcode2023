const input = Bun.file(__dirname + "/input.txt");
let content = await input.text();

const rows = content.split("\n").map((line) => line.split(""));
const numRows = rows.length;
const numCols = rows[0].length;

let totalSum = 0;
const gearValues = {};

for (let row = 0; row < numRows; row++) {
  let adjacentGears = {};
  let currentNumber = 0;
  let hasNonDigitPart = false;

  for (let col = 0; col <= numCols; col++) {
    if (col < numCols && /\d/.test(rows[row][col])) {
      currentNumber = currentNumber * 10 + parseInt(rows[row][col]);

      for (const rowOffset of [-1, 0, 1]) {
        for (const colOffset of [-1, 0, 1]) {
          const newRow = row + rowOffset;
          const newCol = col + colOffset;

          if (
            0 <= newRow &&
            newRow < numRows &&
            0 <= newCol &&
            newCol < numCols
          ) {
            const char = rows[newRow][newCol];

            if (!/\d/.test(char) && char !== ".") {
              hasNonDigitPart = true;
            }

            if (char === "*") {
              adjacentGears[[newRow, newCol]] = true;
            }
          }
        }
      }
    } else if (currentNumber > 0) {
      for (const gear in adjacentGears) {
        if (!gearValues[gear]) {
          gearValues[gear] = [];
        }
        gearValues[gear].push(currentNumber);
      }

      if (hasNonDigitPart) {
        totalSum += currentNumber;
      }

      currentNumber = 0;
      hasNonDigitPart = false;
      adjacentGears = {};
    }
  }
}

totalSum = 0;
for (const gear in gearValues) {
  if (gearValues[gear].length === 2) {
    totalSum += gearValues[gear][0] * gearValues[gear][1];
  }
}

console.log(totalSum);
