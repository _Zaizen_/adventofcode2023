const input = Bun.file(__dirname + "/input.txt");
let content = await input.text();

let totalSum = 0;

let lines = content.split("\n");

const checkDirections = (lines, lineIndex, columnIndex) => {
  const directions = [
    { row: -1, col: 0 }, // Up
    { row: 1, col: 0 }, // Down
    { row: 0, col: -1 }, // Left
    { row: 0, col: 1 }, // Right
    { row: -1, col: -1 }, // Diagonal Up-Left
    { row: -1, col: 1 }, // Diagonal Up-Right
    { row: 1, col: -1 }, // Diagonal Down-Left
    { row: 1, col: 1 }, // Diagonal Down-Right
  ];

  const numRows = lines.length;
  const numCols = lines[0].length;

  for (const direction of directions) {
    let { row, col } = direction;
    let newRow = lineIndex + row;
    let newCol = columnIndex + col;

    // Check boundaries
    if (newRow >= 0 && newRow < numRows && newCol >= 0 && newCol < numCols) {
      const char = lines[newRow][newCol];

      // Check if the character is not a "." or a number
      if (char !== "." && isNaN(parseInt(char))) {
        return true; // Found a non-numeric character
      }
    }
  }

  return false; // No non-numeric character found in any direction
};

let buffNumber = 0;
let foundNeighbour = false;

for (let i = 0; i < lines.length; i++) {
  let line = lines[i];

  let parts = line.split("");

  if (buffNumber > 0) {
    if (foundNeighbour) {
      totalSum += buffNumber;
      foundNeighbour = false;
    }
    buffNumber = 0;
  }

  for (let j = 0; j < parts.length; j++) {
    let part = parts[j];

    part = parseInt(part);

    if (!isNaN(part)) {
      buffNumber = buffNumber * 10 + part;
      if (checkDirections(lines, i, j)) {
        foundNeighbour = true;
      }
    } else {
      if (buffNumber > 0) {
        if (foundNeighbour) {
          totalSum += buffNumber;
          foundNeighbour = false;
        }
        buffNumber = 0;
      }
    }
  }
}

console.log(totalSum);
