const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

let totalSum = 0;

let numbersAsLetters = {
  one: 1,
  two: 2,
  three: 3,
  four: 4,
  five: 5,
  six: 6,
  seven: 7,
  eight: 8,
  nine: 9,
};

let regex = new RegExp("one|two|three|four|five|six|seven|eight|nine|\\d", "g");
// loop all lines of input
for (const line of content.split("\n")) {
  let matchesAll = [];

  // starting from the first character find all matches in the following 4 characters including the first character, then shif to the right by one character and repeat
  for (let i = 0; i < line.length; i++) {
    // get the 4 characters
    let fourCharacters = line.substring(i, Math.min(i + 5, line.length));

    // find if there's a number in the string not at the starting place
    // and substring to before the number
    const numberIndex = fourCharacters.search(/\d/);
    //console.log(numberIndex);
    if (numberIndex !== -1 && numberIndex !== 0) {
      fourCharacters = fourCharacters.substring(0, numberIndex);
    }

    if (numberIndex == 0) {
      matchesAll.push(fourCharacters[0] + "");
      continue;
    }

    // get all the matches
    const matches = fourCharacters.match(regex);

    if (matches === null) {
      continue;
    }
    // append the matches array to the matchesAll array
    for (const match of matches) {
      matchesAll.push(match);
    }
  }

  // remove duplicates that are consecutive
  for (let i = 0; i < matchesAll.length - 1; i++) {
    if (matchesAll[i] === matchesAll[i + 1]) {
      matchesAll.splice(i, 1);
      i--;
    }
  }

  // loop the matches and convert them to numbers
  for (let i = 0; i < matchesAll.length; i++) {
    if (matchesAll[i] in numbersAsLetters) {
      matchesAll[i] = numbersAsLetters[matchesAll[i]];
    }
  }

  const first = "" + matchesAll[0];
  const last = "" + matchesAll[matchesAll.length - 1];
  const composed = first + last;
  const composedNumber = parseInt(composed);
  totalSum += composedNumber;
}

console.log(totalSum);
