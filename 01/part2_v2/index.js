const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

let totalSum = 0;

let numbersAsLetters = {
  one: 1,
  two: 2,
  three: 3,
  four: 4,
  five: 5,
  six: 6,
  seven: 7,
  eight: 8,
  nine: 9,
};

let regexText = "one|two|three|four|five|six|seven|eight|nine";

let regex = new RegExp(regexText + "|\\d", "g");
let reversedRegex = new RegExp(
  regexText.split("").reverse().join("") + "|\\d",
  "g"
);
// loop all lines of input
for (const line of content.split("\n")) {
  let firstMatch = line.match(regex);
  // match right to left

  let reversedLine = line.split("").reverse().join("");
  let secondMatch = reversedLine.match(reversedRegex);

  firstMatch = firstMatch[0];
  secondMatch = secondMatch[0];

  if (firstMatch in numbersAsLetters) {
    firstMatch = numbersAsLetters[firstMatch];
  }

  // reverse the second match
  secondMatch = secondMatch.split("").reverse().join("");

  if (secondMatch in numbersAsLetters) {
    secondMatch = numbersAsLetters[secondMatch];
  }

  const first = "" + firstMatch;
  const last = "" + secondMatch;
  const composed = first + last;
  const composedNumber = parseInt(composed);
  totalSum += composedNumber;
}

console.log(totalSum);
