// read input.txt

const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

let totalSum = 0;

// loop all the lines
for (const line of content.split("\n")) {
  // get the numbers with a regex
  const numbers = line.match(/\d+/g);

  // join all the numbers
  const joined = numbers.join("");

  // split all the numbers
  const splitted = joined.split("");

  // get the first and last number
  const first = splitted[0];
  const last = splitted[splitted.length - 1];
  // create a number composed of the first and last number
  const composed = first + last;
  // cast to number
  const composedNumber = parseInt(composed);
  // add the composed number to the total sum
  totalSum += composedNumber;
}

// print the total sum
console.log(totalSum);
