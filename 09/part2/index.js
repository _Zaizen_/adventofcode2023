const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

/*
content = `0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45`
*/

content = content.trim();

let lines = content.split("\n");

let totalSum = 0;

for (let i = 0; i < lines.length; i++) {
  let inputValues = lines[i].split(" ").map(Number);

  let completedReduce = false;

  let reducedArray = [inputValues];

  let currentArray = inputValues;

  while (!completedReduce) {
    let nextArray = [];
    for (let j = 1; j < currentArray.length; j++) {
      let diff = currentArray[j] - currentArray[j - 1];
      nextArray.push(diff);
    }
    reducedArray.push(nextArray);
    currentArray = nextArray;

    if (currentArray[currentArray.length - 1] == 0) {
      completedReduce = true;
    }
  }

  let nextValue = 0;

  for (let j = reducedArray.length - 2; j >= 0; j--) {
    let nextArray = reducedArray[j];

    // get last value of next array
    let firstValue = nextArray[0];

    nextValue = firstValue - nextValue;
  }

  totalSum += nextValue;
}

console.log(totalSum);
