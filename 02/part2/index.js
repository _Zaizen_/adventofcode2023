const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

let totalSum = 0;

let colorsNames = {
  red: [12, 0],
  green: [13, 1],
  blue: [14, 2],
};

// example row Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green

for (const line of content.split("\n")) {
  // get the Game *: part
  let parts = line.split(":");
  // get the following part splitting by ;
  let gamePart = parts[0];
  let dataPart = parts[1];

  let extractions = dataPart.split(";");

  let results = [0, 0, 0];

  for (const extraction of extractions) {
    let colors = extraction.split(",");

    for (let color of colors) {
      color = color.trim();
      let data = color.split(" ");

      // read the number of cubes for the color
      let number = parseInt(data[0].trim());

      let letters = data[1].trim();

      // get the color number
      let colorNumber = colorsNames[letters];

      if (results[colorNumber[1]] < number) {
        results[colorNumber[1]] = number;
      }
    }
  }

  let totalMult = 1;

  // loop results
  for (const result of results) {
    totalMult *= result;
  }

  // add the game number to the total sum
  totalSum += totalMult;
}

console.log(totalSum);
