const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

content = content.trim();

let lines = content.split("\n");

let time = parseInt(
  lines[0]
    .match(/\d+/g)
    .map((x) => parseInt(x))
    .join("")
);
let distance = parseInt(
  lines[1]
    .match(/\d+/g)
    .map((x) => parseInt(x))
    .join("")
);

let winningCasesCount = 0;

for (let i = 1; i < time; i++) {
  if (distance < i * (time - i)) {
    winningCasesCount++;
  }
}

console.log(winningCasesCount);
