const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

content = content.trim();

let lines = content.split("\n");

let times = lines[0].match(/\d+/g).map((x) => parseInt(x));
let distances = lines[1].match(/\d+/g).map((x) => parseInt(x));

let races = [];

for (let i = 0; i < times.length; i++) {
  races.push([times[i], distances[i]]);
}

let winningCases = []; // in the example [4, 8, 9]

for (let i = 0; i < races.length; i++) {
  let race = races[i];

  let raceTime = race[0];
  let raceDistance = race[1];

  let wins = 0;

  for (let j = 1; j < raceTime; j++) {
    if (raceDistance < j * (raceTime - j)) wins++;
  }

  winningCases.push(wins);
}

let result = winningCases.reduce((a, b) => a * b, 1);

console.log(result);
