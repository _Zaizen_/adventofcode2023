const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

/*
content = `...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....`;
*/

content = content.trim();

let lines = content.split("\n");

let emptyCols = Array(lines[0].length).fill(true); // index of the columns without any #
let emptyRows = []; // index of the rows without any #

let universe = lines.map((line, y) => {
  let rowIsEmpty = true;
  let row = line.split("").map((c, x) => {
    if (c === "#") {
      rowIsEmpty = false;
      emptyCols[x] = false;
      return { x, y, c };
    } else {
      return { x, y, c: "." };
    }
  });
  if (rowIsEmpty) {
    emptyRows.push(y);
  }
  return row;
});

emptyCols = emptyCols.map((v, x) => (v ? x : null)).filter((v) => v !== null);

// for every emptycol index duplicate that col
universe.forEach((row, index) => {
  let counter = 0;
  emptyCols.forEach((colIndex) => {
    row.splice(colIndex + counter, 0, row[colIndex + counter]);
    counter++;
  });
});

let counter = 0;
// duplicate all the empty rows
emptyRows.forEach((y) => {
  universe.splice(
    y + counter,
    0,
    universe[y + counter].map((v) => ({ ...v }))
  );
  counter++;
});

// fix the x and y coordinates
universe.forEach((row, y) => {
  row.forEach((v, x) => {
    v.x = x;
    v.y = y;
  });
});

/*
console.log(emptyCols);
console.log(emptyRows);
*/

/*
// pretty print the universe with column number and row numer
console.log(
  "  " + universe[0].map((v, x) => x.toString().padStart(2, " ")).join("")
);
universe.forEach((row, y) => {
  console.log(
    y.toString().padStart(2, " ") + " " + row.map((v) => v.c).join(" ")
  );
});
*/

// array of positions of the #
let galaxies = universe.flat().filter((v) => v.c === "#");

//console.log(galaxies);

// the only moves allowed are up, down, left, right
const findDistance = (pos1, pos2) => {
  return Math.abs(pos1.x - pos2.x) + Math.abs(pos1.y - pos2.y);
};

let totalDistance = 0;

// for every pair of galaxies find the distance and add it to the total
for (let i = 0; i < galaxies.length; i++) {
  for (let j = i + 1; j < galaxies.length; j++) {
    totalDistance += findDistance(galaxies[i], galaxies[j]);
  }
}

console.log(totalDistance);
