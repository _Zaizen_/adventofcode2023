const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

/*
content = `...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....`;
*/

content = content.trim();

let lines = content.split("\n");

let emptyCols = Array(lines[0].length).fill(true); // index of the columns without any #
let emptyRows = []; // index of the rows without any #

let universe = lines.map((line, y) => {
  let rowIsEmpty = true;
  let row = line.split("").map((c, x) => {
    if (c === "#") {
      rowIsEmpty = false;
      emptyCols[x] = false;
      return { x, y, c };
    } else {
      return { x, y, c: "." };
    }
  });
  if (rowIsEmpty) {
    emptyRows.push(y);
  }
  return row;
});

emptyCols = emptyCols.map((v, x) => (v ? x : null)).filter((v) => v !== null);
/*
console.log(emptyCols);
console.log(emptyRows);
*/
// for every emptycol index add 1 to all the following columns x coordinate
emptyCols.forEach((colIndex) => {
  universe.forEach((row) => {
    for (let x = colIndex; x < row.length; x++) {
      row[x].x += 1000000 - 1;
    }
  });
});

// for every emptyrow index add 1 to all the following rows y coordinate
emptyRows.forEach((rowIndex) => {
  for (let y = rowIndex; y < universe.length; y++) {
    universe[y].forEach((cell) => {
      cell.y += 1000000 - 1;
    });
  }
});

//console.log(universe);

// array of positions of the #
let galaxies = universe.flat().filter((v) => v.c === "#");

//console.log(galaxies);

// the only moves allowed are up, down, left, right
const findDistance = (pos1, pos2) => {
  return Math.abs(pos1.x - pos2.x) + Math.abs(pos1.y - pos2.y);
};

let totalDistance = 0;

// for every pair of galaxies find the distance and add it to the total
for (let i = 0; i < galaxies.length; i++) {
  for (let j = i + 1; j < galaxies.length; j++) {
    totalDistance += findDistance(galaxies[i], galaxies[j]);
  }
}

console.log(totalDistance);
