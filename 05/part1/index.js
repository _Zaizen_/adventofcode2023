const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

let lines = content.split("\n");

let sections = content.split("\r\n\r");

let seeds = lines[0]
  .split(":")[1]
  .trim()
  .split(" ")
  .map((x) => parseInt(x));

let steps = [];

for (let i = 1; i < sections.length; i++) {
  let section = sections[i];
  let lines = section.split("\n");
  let name = lines[1].trim().replace(" map:", "");
  let map = lines.slice(2).map((x) => x.split(" ").map((y) => parseInt(y)));
  steps.push({
    name: name,
    map: map,
  });
}

for (let i = 0; i < seeds.length; i++) {
  let seed = seeds[i];

  for (let j = 0; j < steps.length; j++) {
    let step = steps[j];
    let map = step.map;

    for (let k = 0; k < map.length; k++) {
      let mapRow = map[k];
      let destRangeStart = mapRow[0];
      let sourceRangeStart = mapRow[1];
      let length = mapRow[2];

      if (seed >= sourceRangeStart && seed <= sourceRangeStart + length - 1) {
        seed = destRangeStart + (seed - sourceRangeStart);
        break;
      }
    }
  }

  seeds[i] = seed;
}

let min = Math.min(...seeds);

console.log(min);
