const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

/*
content = `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4`
*/

content = content.trim();

let lines = content.split("\n");
let sections = content.split("\r\n\r"); // to test with content change this to \n\n

// find seeds with regex
let seeds = lines[0].match(/\d+/g).map((x) => parseInt(x));

let seedPairs = [];
for (let i = 0; i < seeds.length; i += 2) {
  seedPairs.push([seeds[i], seeds[i] + seeds[i + 1] - 1]);
}

seeds = seedPairs;

// print seeds like this: 79 - 92, 55 - 67
//console.log(seeds.map(x => x.join(" - ")).join(", ") )

let steps = [];

for (let i = 1; i < sections.length; i++) {
  let section = sections[i];
  let lines = section.split("\n");
  let name = lines[1].trim().replace(" map:", ""); // to test with content change this to [0]
  let map = lines.slice(2).map((x) => x.split(" ").map((y) => parseInt(y))); // to test with content change this to [1]
  steps.push({
    name: name,
    map: map,
  });
}

let minValue = 0;

for (let i = 0; i < seeds.length; i++) {
  //console.log("\n\n" + seeds[i][0] + " - " + seeds[i][1] )

  let seedTuples = [seeds[i]];

  for (let j = 0; j < steps.length; j++) {
    //console.log("\n" + steps[j].name + " map: (" + seedTuples.map(x => x.join(" - ")).join(", ") + ")")

    let step = steps[j];
    let map = step.map;

    let buffTuples = [];
    let consumedRanges = []; // list of (start, end) tuples

    for (let k = 0; k < map.length; k++) {
      let mapRow = map[k];
      let destRangeStart = mapRow[0];
      let sourceRangeStart = mapRow[1];
      let length = mapRow[2];

      let updatedSome = false;

      for (let l = 0; l < seedTuples.length; l++) {
        let seedTuple = seedTuples[l];
        let startingSeed = seedTuple[0];
        let endingSeed = seedTuple[1];

        let min =
          startingSeed < sourceRangeStart ? sourceRangeStart : startingSeed;
        let max =
          endingSeed > sourceRangeStart + length - 1
            ? sourceRangeStart + length - 1
            : endingSeed;

        if (min > max) {
        } else {
          let consumedRange = [min, max];
          let offset = destRangeStart - sourceRangeStart;
          let newRange = [min + offset, max + offset];
          buffTuples.push(newRange);
          consumedRanges.push(consumedRange);
          //console.log(consumedRange.join(" - ") + " -> " + newRange.join(" - "))
          updatedSome = true;
        }
      }

      if (!updatedSome) {
        //console.log("-------")
      }
    }

    // exclude consumed ranges
    for (let k = 0; k < seedTuples.length; k++) {
      let seedTuple = seedTuples[k];
      let startingSeed = seedTuple[0];
      let endingSeed = seedTuple[1];

      // create a union of all consumed ranges
      let union = [];
      for (let l = 0; l < consumedRanges.length; l++) {
        let consumedRange = consumedRanges[l];
        let min = consumedRange[0];
        let max = consumedRange[1];
        union.push(min);
        union.push(max);
      }

      // sort the union
      union.sort((a, b) => a - b);

      // create a list of ranges from the union
      let ranges = [];
      for (let l = 0; l < union.length; l += 2) {
        ranges.push([union[l], union[l + 1]]);
      }

      // exclude ranges from the seed tuple
      for (let l = 0; l < ranges.length; l++) {
        let range = ranges[l];
        let min = range[0];
        let max = range[1];

        if (startingSeed >= min && startingSeed <= max) {
          startingSeed = max + 1;
        }

        if (endingSeed >= min && endingSeed <= max) {
          endingSeed = min - 1;
        }
      }

      if (startingSeed <= endingSeed) {
        buffTuples.push([startingSeed, endingSeed]);
      }
    }

    seedTuples = buffTuples;
  }

  let min = Math.min(...seedTuples.map((x) => x[0]));
  if (min < minValue || minValue == 0) {
    minValue = min;
  }
}

console.log(minValue);
