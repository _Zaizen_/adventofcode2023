const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

const getRowParts = (row) => {
  let parts = row.split("|");
  let firstPart = parts[0];
  let secondPart = parts[1];

  let firstParts = firstPart.split(":");

  let cardNumber = parseInt(firstParts[0].trim().match(/\d+/)[0]);
  let firstNumbers = firstParts[1]
    .trim()
    .match(/\d+/g)
    .map((x) => parseInt(x.trim()));
  let secondNumbers = secondPart
    .trim()
    .match(/\d+/g)
    .map((x) => parseInt(x.trim()));

  return {
    cardNumber: cardNumber,
    firstPart: firstNumbers,
    secondPart: secondNumbers,
  };
};

const getWinningPoints = (firstPart, secondPart) => {
  let winPoints = 0;

  for (const winningNumber of firstPart) {
    if (secondPart.includes(winningNumber)) {
      if (winPoints == 0) {
        winPoints = 1;
      } else {
        winPoints *= 2;
      }
    }
  }

  return winPoints;
};

let totalSum = 0;

for (const line of content.split("\n")) {
  let rowParts = getRowParts(line);

  let winPoints = getWinningPoints(rowParts.firstPart, rowParts.secondPart);

  totalSum += winPoints;
}

console.log(totalSum);
