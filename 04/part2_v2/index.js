const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

let rows = content.split("\n");

let totalCards = 0;

const cardCount = {};

rows.forEach((row, rowIndex) => {
  cardCount[rowIndex + 1] = 1;
});
rows = rows.map((row, rowIndex) => {
  return row.substring(8).split("|");
});
rows = rows.map((row, rowIndex) => {
  return row.map((row, rowIndex) => {
    return row.match(/\d+/g).map((number, numberIndex) => {
      return Number(number);
    });
  });
});
rows = rows.map(([firstPart, secondPart], index) => {
  const currentCard = index + 1;
  const leftPart = new Set([...firstPart]);
  const rightPart = new Set([...secondPart]);

  const intersections = [...rightPart.intersection(leftPart)];

  intersections.forEach((intersection, intersectionIndex) => {
    cardCount[currentCard + 1 + intersectionIndex] += cardCount[currentCard];
  });

  return intersections;
});

let count = Object.values(cardCount).reduce((prev, curr) => prev + curr, 0);

console.log(count);
