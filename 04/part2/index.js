const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();

let totalCards = 0;
const rows = content.split("\n");

// create an array filled with 1 of length rows
let multipliers = Array(rows.length).fill(1);

const getRowParts = (row) => {
  let parts = row.split("|");
  let firstPart = parts[0];
  let secondPart = parts[1];

  let firstParts = firstPart.split(":");

  let cardNumber = parseInt(firstParts[0].trim().match(/\d+/)[0]);
  let firstNumbers = firstParts[1]
    .trim()
    .match(/\d+/g)
    .map((x) => parseInt(x.trim()));
  let secondNumbers = secondPart
    .trim()
    .match(/\d+/g)
    .map((x) => parseInt(x.trim()));

  return {
    cardNumber: cardNumber,
    firstPart: firstNumbers,
    secondPart: secondNumbers,
  };
};

const getWinningPoints = (firstPart, secondPart) => {
  let winPoints = 0;
  let winningRounds = 0;

  for (let winningNumber of firstPart) {
    if (secondPart.includes(winningNumber)) {
      if (winPoints == 0) {
        winPoints = 1;
      } else {
        winPoints *= 2;
      }
      winningRounds += 1;
    }
  }

  return winningRounds;
};

const increaseMultiplier = (rowIndex) => {
  if (rowIndex < rows.length) {
    multipliers[rowIndex] += 1;
  }
};

for (let rowIndex = 0; rowIndex < rows.length; rowIndex++) {
  //console.log("#######################################################")

  let line = rows[rowIndex];

  let rowParts = getRowParts(line);

  //console.log(rowParts)

  let winRounds = getWinningPoints(rowParts.firstPart, rowParts.secondPart);

  for (let i = 0; i < multipliers[rowIndex]; i++) {
    for (let j = 1; j <= winRounds; j++) {
      increaseMultiplier(rowIndex + j);
    }
  }

  //console.log(rows)

  totalCards += multipliers[rowIndex];
}

console.log(totalCards);
