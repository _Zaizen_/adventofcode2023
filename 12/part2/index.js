const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();
/*
content = `???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1
`;
*/
content = content.trim();

let lines = content.split("\n");

// Parse each line to extract springs' status and arrangement
let springs = lines.map((line, index) => {
  let lineBuffer = line.split(" ");
  let springsStatus = lineBuffer[0];
  let springsArrangement = lineBuffer[1].split(",").map((v) => parseInt(v));

  return { springsStatus, springsArrangement };
});

// Create a cache to store computed values for optimization
let cache = {};

// Function to find combinations recursively
function findCombinations(
  springsStatus,
  springsArrangement,
  currentIndex,
  arrangementIndex,
  currentCount
) {
  const key = `${currentIndex},${arrangementIndex},${currentCount}`;

  // Check if the result is already computed and stored in the cache
  if (key in cache) {
    return cache[key];
  }

  // reached the end of the springsStatus
  if (currentIndex === springsStatus.length) {
    // arrangement complete and valid
    if (arrangementIndex === springsArrangement.length && currentCount === 0) {
      return 1;
    } else if (
      arrangementIndex === springsArrangement.length - 1 &&
      springsArrangement[arrangementIndex] === currentCount
    ) {
      return 1;
    } else {
      return 0;
    }
  }

  let totalCombinations = 0;

  let possibleCharacters = [".", "#"];

  // Iterate over possible characters '.' and '#'
  for (const character of possibleCharacters) {
    if (
      springsStatus[currentIndex] === character ||
      springsStatus[currentIndex] === "?"
    ) {
      if (character === "." && currentCount === 0) {
        // Recursive call for '.' when currentCount is 0
        totalCombinations += findCombinations(
          springsStatus,
          springsArrangement,
          currentIndex + 1,
          arrangementIndex,
          0
        );
      } else if (
        character === "." &&
        currentCount > 0 &&
        arrangementIndex < springsArrangement.length &&
        springsArrangement[arrangementIndex] === currentCount
      ) {
        // Recursive call for '.' when currentCount is greater than 0
        totalCombinations += findCombinations(
          springsStatus,
          springsArrangement,
          currentIndex + 1,
          arrangementIndex + 1,
          0
        );
      } else if (character === "#") {
        // Recursive call for '#' when currentCount is incremented
        totalCombinations += findCombinations(
          springsStatus,
          springsArrangement,
          currentIndex + 1,
          arrangementIndex,
          currentCount + 1
        );
      }
    }
  }

  // Store the computed result in the cache
  cache[key] = totalCombinations;

  return totalCombinations;
}

let totalCombinations = 0;

// Iterate over each set of springs and calculate total combinations
springs.forEach((spring) => {
  // Reset the cache for each set of springs
  cache = {};

  totalCombinations += findCombinations(
    [
      spring.springsStatus,
      spring.springsStatus,
      spring.springsStatus,
      spring.springsStatus,
      spring.springsStatus,
    ].join("?"),
    [
      ...spring.springsArrangement,
      ...spring.springsArrangement,
      ...spring.springsArrangement,
      ...spring.springsArrangement,
      ...spring.springsArrangement,
    ],
    0,
    0,
    0
  );
});

console.log(totalCombinations);
