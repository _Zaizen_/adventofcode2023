const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();
/*
content = `...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........`
*/
content = content.trim();

let maze = content.split("\n").map((line) => line.split(""));

const directionsRules = {
  F: [
    { x: 0, y: 1 },
    { x: 1, y: 0 },
  ],
  7: [
    { x: 0, y: 1 },
    { x: -1, y: 0 },
  ],
  J: [
    { x: -1, y: 0 },
    { x: 0, y: -1 },
  ],
  L: [
    { x: 0, y: -1 },
    { x: 1, y: 0 },
  ],
  "-": [
    { x: -1, y: 0 },
    { x: 1, y: 0 },
  ],
  "|": [
    { x: 0, y: -1 },
    { x: 0, y: 1 },
  ],
};

const neighborRules = [
  { x: 0, y: -1 },
  { x: 1, y: 0 },
  { x: 0, y: 1 },
  { x: -1, y: 0 },
];

let flatMaze = [];

for (let i = 0; i < maze.length; i++) {
  for (let j = 0; j < maze[i].length; j++) {
    flatMaze.push({ x: j, y: i, c: maze[i][j] });
  }
}

let startingPos = flatMaze.find((m) => m.c == "S");

let startNeighbours = [];

for (let i = 0; i < neighborRules.length; i++) {
  let neighbor = neighborRules[i];

  let found = flatMaze.find(
    (m) =>
      m.x == startingPos.x + neighbor.x && m.y == startingPos.y + neighbor.y
  );

  if (found) {
    startNeighbours.push(found);
  }
}

let nextNodes = [];

for (let i = 0; i < startNeighbours.length; i++) {
  let neighbor = startNeighbours[i];

  if (directionsRules[neighbor.c]) {
    let possibilities = [];

    for (let j = 0; j < directionsRules[neighbor.c].length; j++) {
      let direction = directionsRules[neighbor.c][j];

      let possibility = {
        x: neighbor.x + direction.x,
        y: neighbor.y + direction.y,
      };

      possibilities.push(possibility);
    }

    let matches = [];

    for (let j = 0; j < possibilities.length; j++) {
      let possibility = possibilities[j];

      if (possibility.x == startingPos.x && possibility.y == startingPos.y) {
        matches.push(possibility);
      }
    }

    if (matches.length) {
      nextNodes.push(neighbor);
    }
  }
}

let startType = null;

for (const direction in directionsRules) {
  let startOptions = [];
  for (let i = 0; i < directionsRules[direction].length; i++) {
    let option = {
      x: startingPos.x + directionsRules[direction][i].x,
      y: startingPos.y + directionsRules[direction][i].y,
    };
    startOptions.push(option);
  }
  let found = 0;
  // check if the point in startOptions match one of nextNodes
  startOptions.forEach((option) => {
    // check is it's the same as nextNodes[0]
    if (option.x == nextNodes[0].x && option.y == nextNodes[0].y) {
      found++;
    }
    if (option.x == nextNodes[1].x && option.y == nextNodes[1].y) {
      found++;
    }
  });

  if (found == 2) {
    startType = direction;
  }
}

startingPos.c = startType;

let visited = [];

visited.push(startingPos);

let queue = [];

startingPos.d = 0;

queue.push(startingPos);

while (queue.length > 0) {
  const node = queue.shift();
  let nextStep = [];

  for (const direction in directionsRules[node.c]) {
    let possibility = {
      x: node.x + directionsRules[node.c][direction].x,
      y: node.y + directionsRules[node.c][direction].y,
    };

    let found = flatMaze.find(
      (m) => m.x == possibility.x && m.y == possibility.y
    );

    if (found) {
      nextStep.push(found);
    }
  }

  let notVisited = [];

  for (let i = 0; i < nextStep.length; i++) {
    let step = nextStep[i];

    if (!visited.includes(step)) {
      notVisited.push(step);
    }
  }

  notVisited.forEach((step) => {
    step.d = node.d + 1;
    visited.push(step);
    queue.push(step);
  });
}

console.log(visited[visited.length - 1].d);
