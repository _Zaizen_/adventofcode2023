const fs = require("fs");

let folders = fs
  .readdirSync("./")
  .filter((folder) => fs.lstatSync(folder).isDirectory());

// sort folders
folders = folders.sort((a, b) => {
  return parseInt(a) - parseInt(b);
});

folders.forEach((folder) => {
  // skip .git
  if (folder === ".git" || folder === ".vscode") return;

  // list subfolders
  const subfolders = fs.readdirSync(`./${folder}`);

  subfolders.forEach(async (subfolder) => {
    const proc = await Bun.spawn(["bun", `./${folder}/${subfolder}/index.js`]);
    const text = (await new Response(proc.stdout).text()).trim();

    const expectedResult = await Bun.file(
      `./${folder}/${subfolder}/result.txt`
    ).text();

    if (text !== expectedResult) {
      console.log(`Test failed for ${folder}/${subfolder}`);
    } else {
      console.log(`Test passed for ${folder}/${subfolder}`);
    }
  });
});
