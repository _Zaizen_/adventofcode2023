const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();
/*
content = `LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)`
*/
content = content.trim();

let lines = content.split("\n");

let pathInstructions = lines[0].split("");

lines = lines.slice(2);

let steps = {};

for (let i = 0; i < lines.length; i++) {
  let row = lines[i].split(" ");
  let key = row[0];
  // remove the first char of row[2]
  let left = row[2].slice(1).slice(0, -1);
  let right = row[3].slice(0, -1);

  steps[key] = [left, right];
}

// number of steps required to reach ZZZ
let numSteps = [];
let currentInstructions = [];
let currentSteps = [];

// current steps is all nodes that ends in "A"
for (let key in steps) {
  if (key.slice(-1) === "A") {
    currentSteps.push(key);
  }
}

for (let i = 0; i < currentSteps.length; i++) {
  numSteps.push(0);
  currentInstructions.push(0);

  while (currentSteps[i].slice(-1) !== "Z") {
    let currentInstruction = currentInstructions[i];
    let instruction = pathInstructions[currentInstruction];
    let nextStep = steps[currentSteps[i]][0];

    if (instruction === "R") {
      nextStep = steps[currentSteps[i]][1];
    }

    currentSteps[i] = nextStep;
    currentInstructions[i] += 1;
    numSteps[i] += 1;

    if (currentInstructions[i] >= pathInstructions.length) {
      currentInstructions[i] = 0;
    }
  }
}

// least common multiple of all numbers in numSteps
let lcm = numSteps[0];

for (let i = 1; i < numSteps.length; i++) {
  let num = numSteps[i];
  let _lcm = lcm;
  while (lcm % num !== 0) {
    lcm += _lcm;
  }
}

console.log(lcm);
