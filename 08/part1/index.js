const input = Bun.file(__dirname + "/input.txt");

let content = await input.text();
/*
content = `RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)`
*/
/*
content = `LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)`
*/
content = content.trim();

let lines = content.split("\n");

let pathInstructions = lines[0].split("");

lines = lines.slice(2);

let steps = {};

for (let i = 0; i < lines.length; i++) {
  let row = lines[i].split(" ");
  let key = row[0];
  // remove the first char of row[2]
  let left = row[2].slice(1).slice(0, -1);
  let right = row[3].slice(0, -1);

  steps[key] = [left, right];
}

// number of steps required to reach ZZZ
let numSteps = 0;

let currentStep = "AAA";
let currentInstruction = 0;

while (currentStep !== "ZZZ") {
  let instruction = pathInstructions[currentInstruction];
  let nextStep = steps[currentStep][0];

  if (instruction === "R") {
    nextStep = steps[currentStep][1];
  }

  currentStep = nextStep;
  currentInstruction += 1;
  numSteps += 1;

  if (currentInstruction >= pathInstructions.length) {
    currentInstruction = 0;
  }
}

console.log(numSteps);
